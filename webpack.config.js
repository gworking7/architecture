'use strict';

const path = require('path');

module.exports = {
    mode: 'development',
    devtool: 'source-map',

    entry: { main: './src/typescript/index.ts' },
    output: {
        path: path.resolve(__dirname, './js'),
        filename: 'main.js',
        library: "Magic"
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.html$/,
                exclude: /node_modules/,
                use: {
                    loader: 'html-loader',
                    options: {
                        attrs: [':data-src'],
                        interpolate: true
                    }
                }
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.html']
    }
};
